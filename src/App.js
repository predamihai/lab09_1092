import React, { Component } from 'react';
import './App.css';
import AddTodo from './AddTodo.js';
import TodoList from './TodoList.js';

class App extends Component {
  constructor(props){
    super(props);
    //de adaugat state
  }
  
  onTodoAdded=(todo)=>{
    console.log(todo);
  }
  render() {
    
    return (
      <div className="App">
     
      <AddTodo onTodoAdded={this.onTodoAdded}/>
     <TodoList title="To do" list={this.onTodoAdded}/>
     <TodoList title="Doing"/>
     <TodoList title="Done"/>
      </div>
    );
  }
}

export default App;
