import React from 'react';
class AddTodo extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            taskName:'',
            priority:'LOW',
            duration:0
        };
    }
    
    //define handler to change data
    handleChangeTaskName =(event)=>{
        
        //change data through setState.. not directly
        this.setState({
            taskName:event.target.value
        });
    }
    
    handleChangePriority =(event)=>{
        this.setState({
            priority:event.target.value
        });
    }
    
    handleChangeDuration =(event)=>{
        this.setState({
            duration:event.target.value
        });
    }
    
    addTodo=(event)=>{
        const todo=this.state;
        this.props.onTodoAdded(todo);
    }

    render(){
        return(
            <div>
                <h1>Add Todo</h1>
                <input 
                    type="text"
                    placeholder="Task Name"
                    value={this.state.taskName}
                    onChange={this.handleChangeTaskName}
                />
                
                <select 
                value={this.state.priority}
                onChange={this.handleChangePriority}
                >
                    <option>LOW</option>
                    <option>MEDIUM</option>
                    <option>HIGH</option>
                </select>
                
                <input 
                    type="number"
                    placeholder="Duration"
                    value={this.state.duration}
                    onChange={this.handleChangeDuration}
                />
                <button onClick={this.addTodo}>Add</button>    
            </div>
            );
    }

}
 export default AddTodo; 
