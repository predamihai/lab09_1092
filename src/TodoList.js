import React from 'react';
class TodoList extends React.Component{
    render(){
        return(
        <div>
            <h1>{this.props.title}</h1>
            <h2>List</h2>
                {this.props.list}
        </div>
        );
    }
}
export default TodoList;
